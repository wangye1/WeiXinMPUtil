package com.wangye.mp.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.wangye.mp.MPConfig;
import com.wangye.mp.constant.MPConstantUtil;
import com.wangye.mp.model.request.TemplateMessage;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by 王叶 on 2018/3/15.
 */
public class TemplateMessageUtilTest {

    private String APPID ;
    private String APPSECRET;
    private String WEIXIN_TOKEN;
    private String WEIXIN_URL;
    private String ACCESS_TOKEN;

    @Before
    public void before(){
        WEIXIN_URL="https://api.weixin.qq.com";
        ACCESS_TOKEN ="7_zgchl7-rznZQNiQh-z7mnweHX8Lm8eotyAezktP-jRgl7rsYGUMv5AUpZfbhb_gL4MDcK_n7LFbj0Ql9u3LX-8qLjilHgHQZquCR2ZbgOp993kgIAjiNOddlTc-0yYIkBrDAVPinCbdvh5UyAALbAJANNY";
    }

    @Test
    public void testGetAllTemplate() throws Exception {
        System.out.println(MPConfig.getAPPID());
        JSON json = TemplateMessageUtil.getAllTemplate(MPConstantUtil.DEFAULT_WEIXIN_URL,ACCESS_TOKEN);
        System.out.print(json);
    }

    public void testSendModelMessage() throws Exception {
//王叶  安正飞鸟 公众号 openID = oKxsHwLQrqpPstndieNB5GCjWGxw
        String templateId;
        String openId;
        JSONObject data;
        openId = "oKxsHwLQrqpPstndieNB5GCjWGxw";
        templateId = "jXxoHy7y_5yzFt_-xy76ir_0ZYwW0N0S-7wilgj9QVA";  // 购买成功 模板ID
        data = new JSONObject(8);
        data.put("first", TemplateMessage.getDateLable("古墩路有人被困 \n ","#173177"));
        data.put("keyword1",TemplateMessage.getDateLable("火灾","#173177"));
        data.put("keyword2",TemplateMessage.getDateLable("杭州市西湖区西湖中队","#173177"));
        data.put("remark",TemplateMessage.getDateLable(" \n该地发生火灾，请立即前往救援","#173177"));
        TemplateMessage message = new TemplateMessage();
        message.setTouser(openId);
        message.setTemplateId(templateId);
        message.setData(data);
        JSON response = TemplateMessageUtil.sendModelMessage(WEIXIN_URL,ACCESS_TOKEN,message);
        System.out.print(response);
    }

}