package com.wangye.commom;

import com.wangye.mp.constant.MPConstantUtil;

import java.util.List;

/**
 * Created by 王叶 on 2018/3/14.
 */
public class EmptyUtil {
    public static boolean isNull(Object obj) {
        return obj == null;
    }

    public static boolean isEmpty(Object obj) {
        return obj == null || MPConstantUtil.EMPTY_STRING.equals(obj);
    }

    public static boolean isEmptyTrim(Object obj) {
        return isEmpty(obj) || MPConstantUtil.EMPTY_STRING.equals(obj.toString().trim());
    }

    public static boolean isEmptyStringTrim(String obj){
        return isEmpty(obj) || MPConstantUtil.EMPTY_STRING.equals(obj.trim());
    }

    public static boolean isEmpty(Object[] obj) {
        return obj == null || obj.length == 0;
    }

    public static boolean isEmpty(List<?> list) {
        return list == null || list.size() == 0;
    }

}
