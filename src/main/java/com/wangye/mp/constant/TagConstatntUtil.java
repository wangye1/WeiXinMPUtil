package com.wangye.mp.constant;

/**
 * Created by 王叶 on 2018/3/15.
 * 标签 常量类
 */
public class TagConstatntUtil {

    /**
     * 用户 标签相关
     */
    //创建标签
    public static final String TAGS_CREATE_URL = "/cgi-bin/tags/create";
    //获取公众号已创建的标签
    public static final String TAGS_GET_URL = "/cgi-bin/tags/get";
    //编辑标签
    public static final String TAGS_UPDATE_URL = "/cgi-bin/tags/update";
    //删除标签
    public static final String TAGS_DELETE_URL = "/cgi-bin/tags/delete";
    //获取标签下粉丝列表
    public static final String TAGS_USER_GET_URL = "/cgi-bin/user/tag/get";


}
