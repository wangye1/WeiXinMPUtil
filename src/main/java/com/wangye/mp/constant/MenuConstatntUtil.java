package com.wangye.mp.constant;

/**
 * Created by 王叶 on 2018/3/15.
 * 菜单 常量类
 */
public class MenuConstatntUtil {

    /**
     *  公众号 菜单相关接口
     */

    //自定义菜单查询接口
    public static final String MENU_GET_URL = "/cgi-bin/menu/get";

    //自定义菜单创建接口
    public static final String MENU_CREATE_URL = "/cgi-bin/menu/create";

    //自定义菜单删除接口
    public static final String MENU_DELETE_URL = "/cgi-bin/menu/delete";

    //个性化菜单创建接口
    public static final String MENU_CONDITIONAL_CREATE_URL = "/cgi-bin/menu/addconditional";

    //个性化菜单删除接口
    public static final String MENU_CONDITIONAL_DELETE_URL = "/cgi-bin/menu/delconditional";

    //测试个性化菜单匹配结果 接口
    public static final String MENU_TRY_MATCH_URL = "/cgi-bin/menu/trymatch";

}
