package com.wangye.mp.constant;

/**
 * Created by 王叶 on 2018/3/15.
 * 模板常量类
 */
public class TemplateConstantUtil {
    /**
     * 模板消息 URL
     */
    //设置所属行业 URL
    public static final String TEMPLATE_SET_INDUSTRY_URL = "/cgi-bin/template/api_set_industry";

    //获取设置的行业信息 URL
    public static final String TEMPLATE_GET_INDUSTRY_URL = "/cgi-bin/template/get_industry";


    /**
     * 获得模板ID URL
     * --从行业模板库选择模板到帐号后台，
     * 获得模板ID的过程可在微信公众平台后台完成。
     * 为方便第三方开发者，提供通过接口调用的方式来获取模板ID，
     */
    public static final String TEMPLATE_ADD_TEMPLATE_URL = "/cgi-bin/template/api_add_template";
    //获取模板列表 URL
    public static final String TEMPLATE_GET_ALL_URL = "/cgi-bin/template/get_all_private_template";
    //删除模板 URL
    public static final String TEMPLATE_DEL_URL = "/cgi-bin/template/del_private_template";


    //发送模板消息 URL
    public static final String TEMPLATE_SEND_MESSAGE_URL = "/cgi-bin/message/template/send";


    /**
     * 模板消息发送完成 推送状态
     */
    //送达成功
    public static final String TEMPLATE_SEND_STATUS_SUCCESS = "success";
    //用户拒收（用户设置拒绝接收公众号消息）而失败
    public static final String TEMPLATE_SEND_STATUS_USER_BLOCK = "failed:user block";
    //由于其他原因失败
    public static final String TEMPLATE_SEND_STATUS_SYSTEM_FAILED = "failed:user block";
}
