package com.wangye.mp.constant;

/**
 * Created by 王叶 on 2018/3/14.
 * 微信公众平台 （Media Platform） 公用 常量类
 */
public class MPConstantUtil {


    public final static String  DEFAULT_CHARSET = "UTF-8";      //默认编码格式

    public final static String  EMPTY_STRING = "";      //默认 空字符串

    /*
    #1. 通用域名(api.weixin.qq.com)，使用该域名将访问官方指定就近的接入点；
    #2. 上海域名(sh.api.weixin.qq.com)，使用该域名将访问上海的接入点；
    #3. 深圳域名(sz.api.weixin.qq.com)，使用该域名将访问深圳的接入点；
    #4. 香港域名(hk.api.weixin.qq.com)，使用该域名将访问香港的接入点。
     */
    public  static final String DEFAULT_WEIXIN_URL = "https://api.weixin.qq.com";

    //服务端 access_token 有效期 微信默认2小时 7200s 本服务设置有效期为 6000s
    public static final Integer ACCESS_TOKEN_VALID_TIME = 6000;

    //获取 基础服务 access_token
    public static final String ACCESS_TOKEN_URL = "/cgi-bin/token";

    //获取 网页授权 access_token
    public static final String WEB_ACCESS_TOKEN_URL = "/sns/oauth2/access_token";

    //获取 微信 JS SDK 授权
    public static final String JS_API_TICKET_URL = "/cgi-bin/ticket/getticket";

}
