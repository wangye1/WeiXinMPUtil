package com.wangye.mp.model.request;

/**
 * Created by wangy on 2017/11/22.
 * 微信 图文素材 model
 */
public class MaterialNews {
    //标题
    private String title;

    //图文消息的封面图片素材id（必须是永久 media_ID）
    private String thumbMediaId;

    //作者
    private String authod;

    //图文消息的摘要，仅有单图文消息才有摘要，多图文此处为空
    private String digest;

    //是否显示封面，0为false，即不显示，1为true，即显示
    private String showCoverPic;

    //图文消息的具体内容，支持HTML标签，必须少于2万字符，小于1M，且此处会去除JS
    private String content;

    //图文消息的原文地址，即点击“阅读原文”后的URL
    private String contentSourceUrl;

    //是否打开评论，0不打开，1打开
    private long needOpenComment;

    //是否粉丝才可评论，0所有人可评论，1粉丝才可评论
    private long onlyFansCanComment;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getThumbMediaId() {
        return thumbMediaId;
    }

    public void setThumbMediaId(String thumbMediaId) {
        this.thumbMediaId = thumbMediaId;
    }

    public String getAuthod() {
        return authod;
    }

    public void setAuthod(String authod) {
        this.authod = authod;
    }

    public String getDigest() {
        return digest;
    }

    public void setDigest(String digest) {
        this.digest = digest;
    }

    public String getShowCoverPic() {
        return showCoverPic;
    }

    public void setShowCoverPic(String showCoverPic) {
        this.showCoverPic = showCoverPic;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContentSourceUrl() {
        return contentSourceUrl;
    }

    public void setContentSourceUrl(String contentSourceUrl) {
        this.contentSourceUrl = contentSourceUrl;
    }

    public long getNeedOpenComment() {
        return needOpenComment;
    }

    public void setNeedOpenComment(long needOpenComment) {
        this.needOpenComment = needOpenComment  & ((1L << 32) - 1);
    }

    public long getOnlyFansCanComment() {
        return onlyFansCanComment;
    }

    public void setOnlyFansCanComment(long onlyFansCanComment) {
        this.onlyFansCanComment = onlyFansCanComment  & ((1L << 32) - 1);
    }
}
