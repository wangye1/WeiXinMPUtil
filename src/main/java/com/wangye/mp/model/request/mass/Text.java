package com.wangye.mp.model.request.mass;

/**
 * Created by wangy on 2017/11/22.
 * 文本消息
 */
public class Text {

    public static final String TYPE = "text";

    //用于设定图文消息的接收者
    private Filter filter;

    //文本内容
    private String content;

    public Filter getFilter() {
        return filter;
    }

    public void setFilter(Filter filter) {
        this.filter = filter;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
