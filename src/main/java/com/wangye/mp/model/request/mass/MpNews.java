package com.wangye.mp.model.request.mass;

/**
 * Created by wangy on 2017/11/22.
 * 图文信息
 */
public class MpNews{

    //用于设定图文消息的接收者
    private Filter filter;
    /*
     *图文消息被判定为转载时，是否继续群发。1为继续群发（转载），0为停止群发。该参数默认为0。
     */
    private String sendIgnoreReprint;

    //群发的消息类型，图文消息为mpnews
    public static final String TYPE = "mpnews";

    private String mediaId;

    public String getMediaId() {
        return mediaId;
    }

    public void setMediaId(String mediaId) {
        this.mediaId = mediaId;
    }

    public String getSendIgnoreReprint() {
        return sendIgnoreReprint;
    }

    public void setSendIgnoreReprint(String sendIgnoreReprint) {
        this.sendIgnoreReprint = sendIgnoreReprint;
    }

    public Filter getFilter() {
        return filter;
    }

    public void setFilter(Filter filter) {
        this.filter = filter;
    }
}
