package com.wangye.mp.model.request;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

/**
 * Created by wangy on 2017/9/22.
 * 微信公众号 消息模板
 */
public class TemplateMessage {
    private String touser;                  //OPENID 接收消息模板 的用户openid

    private String templateId;             //消息模板ID

    private String url;                      //模板跳转 链接

    private JSONObject miniprogram;         //跳小程序所需数据，不需跳小程序可不用传该数据

    private JSONObject data;                 //模板数据

    public String getTouser() {
        return touser;
    }

    public void setTouser(String touser) {
        this.touser = touser;
    }

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public JSONObject getMiniprogram() {
        return miniprogram;
    }

    public void setMiniprogram(JSONObject miniprogram) {
        this.miniprogram = miniprogram;
    }

    public JSONObject getData() {
        return data;
    }

    public void setData(JSONObject data) {
        this.data = data;
    }

    public static JSON getDateLable(String text, String color){
        JSONObject jsonObject = new JSONObject(2);
        jsonObject.put("value",text);
        jsonObject.put("color",color);
        return jsonObject;
    }
}
