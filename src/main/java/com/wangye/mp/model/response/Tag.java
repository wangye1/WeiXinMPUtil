package com.wangye.mp.model.response;

/**
 * Created by wangy on 2017/11/22.
 * 微信 用户 标签
 */
public class Tag {
    //标签id
    private String id;
    //标签名
    private String name;
    //标签下粉丝人数
    private Integer count;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
