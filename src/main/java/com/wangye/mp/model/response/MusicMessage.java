package com.wangye.mp.model.response;

/**
 * Created by SEELE on 2017/7/12.
 */
public class MusicMessage extends BaseMessage {
    //音乐
    private Music music;

    public Music getMusic() {
        return music;
    }

    public void setMusic(Music music) {
        this.music = music;
    }
}
