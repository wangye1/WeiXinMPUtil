package com.wangye.mp.model.response;

/**
 * Created by SEELE on 2017/7/12.
 * 微信 基本信息类
 */
public class BaseMessage {
    //接收方帐号，用户的OPEN_ID
    private String  ToUserName;
    //开发者的微信号
    private String FromUserName;
    //创建时间
    private long CreateTime;
    //消息类型
    private String MsgType;

    public String getToUserName() {
        return ToUserName;
    }

    public void setToUserName(String toUserName) {
        ToUserName = toUserName;
    }

    public String getFromUserName() {
        return FromUserName;
    }

    public void setFromUserName(String fromUserName) {
        FromUserName = fromUserName;
    }

    public long getCreateTime() {
        return CreateTime;
    }

    public void setCreateTime(long createTime) {
        CreateTime = createTime;
    }

    public String getMsgType() {
        return MsgType;
    }

    public void setMsgType(String msgType) {
        MsgType = msgType;
    }
}
