package com.wangye.mp.model.response;

/**
 * Created by SEELE on 2017/7/12.
 * 视屏/短视频  消息类
 */
public class VideoMessage extends BaseMessage {
    //视屏/短视频
    private Video video;

    public Video getVideo() {
        return video;
    }

    public void setVideo(Video video) {
        this.video = video;
    }
}
