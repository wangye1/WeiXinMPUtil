package com.wangye.mp.model.response;

/**
 * Created by SEELE on 2017/7/12.
 *  文本消息类
 */
public class TextMessage extends BaseMessage {
    //文本消息内容
    private String Content;

    public String getContent() {
        return Content;
    }

    public void setContent(String content) {
        Content = content;
    }
}
