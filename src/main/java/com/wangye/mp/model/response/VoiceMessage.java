package com.wangye.mp.model.response;

/**
 * Created by SEELE on 2017/7/12.
 * 语音消息类
 */
public class VoiceMessage extends BaseMessage {
    //语音
    private Voice video;

    public Voice getVideo() {
        return video;
    }

    public void setVideo(Voice video) {
        this.video = video;
    }
}
