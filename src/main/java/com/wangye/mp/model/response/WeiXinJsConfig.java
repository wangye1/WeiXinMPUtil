package com.wangye.mp.model.response;

/**
 * Created by wangy on 2017/9/13.
 * 调用的微信 JS-SDK 的权限验证配置
 */
public class WeiXinJsConfig {

    private String appId;       //公众号的唯一标识

    private Long timeStamp;     //生成签名的时间戳

    private String nonceStr;    //生成签名的随机串

    private String signature ;   //签名

    private String[] jsApiList; //需要使用的JS接口列表

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public Long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getNonceStr() {
        return nonceStr;
    }

    public void setNonceStr(String nonceStr) {
        this.nonceStr = nonceStr;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String[] getJsApiList() {
        return jsApiList;
    }

    public void setJsApiList(String[] jsApiList) {
        this.jsApiList = jsApiList;
    }
}
