package com.wangye.mp.model.response;

/**
 * Created by SEELE on 2017/7/12.
 */
public class Image {
    //图片 媒体Id
    private String MediaId;

    public String getMediaId() {
        return MediaId;
    }

    public void setMediaId(String mediaId) {
        MediaId = mediaId;
    }
}
