package com.wangye.mp.model.response;

/**
 * Created by SEELE on 2017/7/12.
 */
public class Voice {
    //消息媒体文件Id
    private String MediaId;

    public String getMediaId() {
        return MediaId;
    }

    public void setMediaId(String mediaId) {
        MediaId = mediaId;
    }
}
