package com.wangye.mp.model.response;

/**
 * Created by SEELE on 2017/7/12.
 * 图片消息类
 */
public class ImageMessage extends BaseMessage {
    private Image image;

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }
}
