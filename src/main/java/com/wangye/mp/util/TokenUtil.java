package com.wangye.mp.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.wangye.http.util.HttpClientUtil;
import com.wangye.mp.constant.MPConstantUtil;

/**
 * Created by 王叶 on 2018/3/14.
 */
public class TokenUtil {

    /**
     * 获取 基础服务 access_token
     * @param appid     第三方用户唯一凭证
     * @param secret    第三方用户唯一凭证密钥，即appsecret
     * @param weixinUrl 公众平台接口域名
     * @return  返回String则是 access_token   返回null则 获取token错误
     */
    public static JSON getAccessToken(String appid, String secret, String weixinUrl){
        //设置 请求参数
       JSONObject param = new JSONObject(3);
        param.put("grant_type","client_credential");//获取access_token填写client_credential
        param.put("appid",appid);
        param.put("secret",secret);
        //执行get 请求
        return HttpClientUtil.executeGet(weixinUrl+ MPConstantUtil.ACCESS_TOKEN_URL,param);
    }
}
