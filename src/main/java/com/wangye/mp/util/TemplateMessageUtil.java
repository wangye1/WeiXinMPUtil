package com.wangye.mp.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.wangye.commom.EmptyUtil;
import com.wangye.http.util.HttpClientUtil;
import com.wangye.mp.constant.MPConstantUtil;
import com.wangye.mp.constant.TemplateConstantUtil;
import com.wangye.mp.model.request.TemplateMessage;

/**
 * Created by 王叶 on 2018/3/14.
 */
public class TemplateMessageUtil {

    /**
     * 获取模板列表
     * @param weixinUrl
     * @param accessToken
     * @return
     */
    public static JSON getAllTemplate(String weixinUrl, String accessToken){
        JSONObject param = new JSONObject(1);
        param.put("access_token",accessToken);
        JSONObject jsonObject =  HttpClientUtil.executeGet(weixinUrl+ TemplateConstantUtil.TEMPLATE_GET_ALL_URL,param);
        return jsonObject;
    }


    /**
     * 发送模板消息
     * @param weixinUrl
     * @param accessToken
     * @param message
     * @return
     */
    public static JSON sendModelMessage(String weixinUrl, String accessToken, TemplateMessage message) {
        JSONObject paramJson = new JSONObject();
        paramJson.put("touser",message.getTouser());
        paramJson.put("template_id",message.getTemplateId());
        if (!EmptyUtil.isEmpty(message.getUrl())) {
            paramJson.put("url",message.getUrl() );
        }
        if (!EmptyUtil.isEmpty(message.getMiniprogram())) {
            paramJson.put("miniprogram",message.getMiniprogram() );
        }
        paramJson.put("data",message.getData());
        return HttpClientUtil.executePostByJson(weixinUrl+ TemplateConstantUtil.TEMPLATE_SEND_MESSAGE_URL+"?access_token="+accessToken,paramJson);
    }
}
